typedef struct digit_s
{
	char whole[128] = {0};
	char fraction[58] = {0};
} digit_t;


bool add(digit_t* a, digit_t* b);

void printDigit(digit_t* a);
#include <cstdint>
#include <cstdio>

typedef struct digit_s
{
	uint8_t whole[128] = {0};
	uint8_t fraction[58] = {0};
} digit_t;

bool add(digit_t* a, digit_t* b)
{
	bool ret = true;

	uint32_t carryBitsFrac = 0;
	uint32_t carryBitsWhole = 0;
	for (uint32_t i = 0; i < sizeof(digit_s::fraction); i++)
	{
		carryBitsFrac += a->fraction[i] + b->fraction[i];
		a->fraction[i] = (carryBitsFrac & 0xff);
		carryBitsFrac = carryBitsFrac >> 8;

		carryBitsWhole = a->whole[i] + b->whole[i];
		a->whole[i] = (carryBitsWhole & 0xff);
		carryBitsWhole = carryBitsWhole >> 8;

		if (carryBitsWhole > 0 || carryBitsFrac > 0)
		{
			ret = false;
		}
	}

	return ret;
}

void printDigit(digit_t* a)
{
	uint32_t uval32_1 = 0;
	uint32_t uval32_2 = 0;

	for (uint32_t i = 0; i < sizeof(digit_t::whole); i++)
	{
		uval32_1 = a->whole[0];
		while (uval32_1 != 0)
		{
			while (true)
			{
				uval32_1 += a->whole[i];
				if (uval32_1 < uval32_2)
				{
					break;
				}
				i++;
			}
			while (true)
			{
				uint8_t currentChar = (uval32_2 % 10);
				printf("%c", currentChar);
				uval32_2 /= 10;
				if (uval32_2 == 0)
				{
					break;
				}
			}
		}

	}

	for (uint32_t i = 0; i < sizeof(digit_t::fraction); i++)
	{
		uval32_1 = a->fraction[0];
		while (uval32_1 != 0)
		{
			while (true)
			{
				uval32_1 += a->fraction[i];
				if (uval32_1 < uval32_2)
				{
					break;
				}
				i++;
			}
			while (true)
			{
				uint8_t currentChar = (uval32_2 % 10);
				printf("%c", currentChar);
				uval32_2 /= 10;
				if (uval32_2 == 0)
				{
					break;
				}
			}
		}

	}
}
